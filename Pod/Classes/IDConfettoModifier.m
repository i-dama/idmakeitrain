//
//  ConfettiModifier.m
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "IDConfettoModifier.h"
#import "IDConfettoVerticalTranslationModifier.h"
#import "IDRotationModifier.h"
#import "IDHorizontalTranslationModifier.h"

@implementation IDConfettoModifier

- (id) init{
    self = [super init];
    if(self){
        self.delay = (arc4random() % 400)/100.0;
    }
    return self;
}

- (void)modifyLayer:(CALayer *)layer atRelativeTime:(double)relativeTime{
    NSAssert(NO, @"Sending a message to an abstract class!");
}

- (void)modifyLayer:(CALayer *)layer atTime:(double)time{
    double relativeTime = time - self.delay;
    if(relativeTime > 0){
        [self modifyLayer:layer atRelativeTime:relativeTime];
    }
}

+ (NSArray *)randomStandardModifiers{
    return @[[IDRotationModifier modifierWithRandomAnglePerSecondInRange:NSMakeRange(1, 1.5)], [IDConfettoVerticalTranslationModifier modifierWithRandomTranslationWithinRange:NSMakeRange(70, 150)], [IDHorizontalTranslationModifier modifierWithRandomTranslationFrom:-10 to:10]];
}

@end
