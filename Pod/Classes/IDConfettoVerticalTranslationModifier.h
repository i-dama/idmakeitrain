//
//  ConfettoVerticalTranslationModifier.h
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDConfettoModifier.h"

@interface IDConfettoVerticalTranslationModifier : IDConfettoModifier

+ (instancetype)modifierWithRandomTranslationWithinRange:(NSRange)range;
+ (instancetype)modifierWithTranslation:(CGFloat)translation;

@end
