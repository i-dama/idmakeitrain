//
//  UIView+Confetti.h
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDConfettiCoordinator.h"

@interface UIView (Confetti)

//completion block called when the last confetto leves the view
- (void)makeItRainWithCompletion:(void (^)())completion;
- (void)cleanUpTheMess;

//private
+ (IDConfettiCoordinator *)coordinatorForView:(UIView *)view;

@end
