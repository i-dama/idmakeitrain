//
//  ConfettiConfiguration.h
//  VriendenLoterij
//
//  Created by Dama on 04/08/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IDConfettiConfiguration : NSObject

+ (instancetype)sharedConfiguration;

@property (assign, nonatomic) CGFloat confettiWidth;
@property (assign, nonatomic) CGFloat confettiHeight;
@property (strong, nonatomic) NSArray *confettiImages;
@property (assign, nonatomic) NSInteger numberOfConfettos;

@end
