//
//  HorizontalTranslationModifier.h
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "IDConfettoModifier.h"
#import <UIKit/UIKit.h>

@interface IDHorizontalTranslationModifier : IDConfettoModifier

+ (instancetype)modifierWithRandomTranslationFrom:(CGFloat)from to:(CGFloat)to;
+ (instancetype)modifierWithTranslation:(CGFloat)translation;

@end
