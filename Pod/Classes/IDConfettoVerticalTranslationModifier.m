//
//  ConfettoVerticalTranslationModifier.m
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "IDConfettoVerticalTranslationModifier.h"

@interface IDConfettoVerticalTranslationModifier ()
@property (nonatomic, assign) CGFloat stepYTranslation;
@end

@implementation IDConfettoVerticalTranslationModifier

+ (instancetype)modifierWithRandomTranslationWithinRange:(NSRange)range{
    return [self modifierWithTranslation:(range.location + (arc4random() % range.length))];
}

+ (instancetype)modifierWithTranslation:(CGFloat)translation{
    IDConfettoVerticalTranslationModifier *modifier = [IDConfettoVerticalTranslationModifier new];
    modifier.stepYTranslation = translation;
    return modifier;
}

- (void)modifyLayer:(CALayer *)layer atRelativeTime:(double)relativeTime{
    layer.transform = CATransform3DConcat(layer.transform, CATransform3DMakeTranslation(0, relativeTime * self.stepYTranslation, 0));
}

@end
