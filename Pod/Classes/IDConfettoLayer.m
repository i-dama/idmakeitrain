//
//  ConfettiLayer.m
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "IDConfettoLayer.h"
#import "IDConfettiConfiguration.h"

#define kWidth ([IDConfettiConfiguration sharedConfiguration].confettiWidth)
#define kHeight ([IDConfettiConfiguration sharedConfiguration].confettiHeight)

@interface IDConfettoLayer ()
@property (nonatomic, strong) NSArray *confettiModifiers;
@end

@implementation IDConfettoLayer

- (id)initWithFrame:(CGRect)frame{
    self = [super init];
    if(self){
        self.frame = frame;
        [self setupSublayersAndMask];
    }
    return self;
}

+ (instancetype)createConfettoWithRandomizedModifiersAndPositionForView:(UIView *)view{
    CGPoint start = CGPointMake(arc4random() % ((NSInteger)(CGRectGetWidth(view.frame) - kWidth) ) , - kHeight);
    return [self createConfettoWithModifiers:[IDConfettoModifier randomStandardModifiers] atStartPosition:start];
}

+ (instancetype)createConfettoWithModifiers:(NSArray *)modifiers atStartPosition:(CGPoint)startPosition{
    IDConfettoLayer *layer = [[IDConfettoLayer alloc] initWithFrame:CGRectMake(startPosition.x, startPosition.y, kWidth, - kHeight)];
    layer.confettiModifiers = modifiers;
    return layer;
}

#pragma mark - setup

- (void)setupSublayersAndMask{
    self.contents = (__bridge id) [IDConfettoLayer randomImage].CGImage;
    self.contentsGravity = kCAGravityResizeAspect;
}

+ (UIImage *)randomImage{
    NSArray *images = [IDConfettiConfiguration sharedConfiguration].confettiImages;
    if (images.count > 0) {
        int rand = arc4random() % images.count;
        return [images objectAtIndex:rand];
    } else {
        return nil;
    }

}
@end
