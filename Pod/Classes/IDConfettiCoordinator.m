//
//  ConfettiModifyingCoordinator.m
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "IDConfettiCoordinator.h"
#import <POP/POP.h>
#import "IDConfettoLayer.h"
#import "UIView+Confetti.h"

NSString *const ConfettiCoordinatorAnimationKey = @"makinItRain";

@interface IDConfettiCoordinator ()
@property (nonatomic, strong) NSArray *confetti;
@property (nonatomic, assign) CGFloat relativeTime;
@property (nonatomic, weak) UIView *assignedView;
@property (copy) void (^completion)();
@end

@implementation IDConfettiCoordinator

+ (instancetype)confettiCoordinatorWithConfetti:(NSArray *)confetti completion:(void (^)())completion{
    IDConfettiCoordinator *c = [IDConfettiCoordinator new];
    c.confetti = confetti;
    c.completion = completion;
    return c;
}

- (void)startInView:(UIView *)view{
    self.assignedView = view;
    [view pop_removeAnimationForKey:ConfettiCoordinatorAnimationKey];
    POPAnimatableProperty *custom = [POPAnimatableProperty propertyWithName:@"relativeTime" initializer:^(POPMutableAnimatableProperty *prop) {
        // read value
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [UIView coordinatorForView:obj].relativeTime;
        };
        // write value
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
           [UIView coordinatorForView:obj].relativeTime  = values[0];
        };
        // dynamics threshold
        prop.threshold = 0.01;
    }];
    
    static const CGFloat maxDuration = 30;
    
    POPBasicAnimation *animation = [POPBasicAnimation linearAnimation];
    animation.property = custom;
    animation.duration = maxDuration;
    animation.fromValue = @(0);
    animation.removedOnCompletion = YES;
    animation.toValue = @(maxDuration);
    
    [animation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        if(finished){
            [view cleanUpTheMess];
        }
    }];
    
    [view pop_addAnimation:animation forKey:ConfettiCoordinatorAnimationKey];
}

- (void)endInView:(UIView *)view{
    [view pop_removeAnimationForKey:ConfettiCoordinatorAnimationKey];
}

- (void)setRelativeTime:(CGFloat)relativeTime{
    _relativeTime = relativeTime;
    __block BOOL hasVisibleConfetti = NO;
    [self.confetti enumerateObjectsUsingBlock:^(IDConfettoLayer *layer, NSUInteger idx, BOOL *stop) {
        layer.transform = CATransform3DIdentity;
        [layer.confettiModifiers enumerateObjectsUsingBlock:^(IDConfettoModifier *modifier, NSUInteger idx, BOOL *stop) {
            [modifier modifyLayer:layer atTime:relativeTime];
        }];
        if(CGRectIntersectsRect(layer.frame, layer.superlayer.bounds)){
            hasVisibleConfetti = YES;
        }
    }];
    //visible at least 5 secs
    if(relativeTime > 5 && !hasVisibleConfetti){
        if(self.assignedView){
            [self.assignedView cleanUpTheMess];
            self.assignedView = nil;
            if(self.completion){
                self.completion();
            }
        }
    }
}
@end
