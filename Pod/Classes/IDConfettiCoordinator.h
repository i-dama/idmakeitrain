//
//  ConfettiModifyingCoordinator.h
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IDConfettiCoordinator : NSObject

+ (instancetype)confettiCoordinatorWithConfetti:(NSArray *)confetti completion:(void (^)())completion;

- (void)startInView:(UIView *)view;
- (void)endInView:(UIView *)view;


@end
