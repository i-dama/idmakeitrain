//
//  ConfettiModifier.h
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IDConfettoModifier : NSObject

//apply some kind of a transform on the layer at relative time.
//relative time:
// 0 - do nothing
// 1 - apply 1 "dose" of transform - if a modifier is in charge of moving the layer by 100px, a value of 1 means that it should move the layer by 100 px
//2 - apply 2 "dose" of transform - if a modifier is in charge of moving the layer by 100px, a value of 2 means that it should move the layer by 200 px
- (void)modifyLayer:(CALayer *)layer atRelativeTime:(double)relativeTime;

@property (nonatomic, assign) CGFloat delay;
- (void)modifyLayer:(CALayer *)layer atTime:(double)time;

+ (NSArray *)randomStandardModifiers;

@end
