//
//  RotationModifier.m
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "IDRotationModifier.h"

@interface IDRotationModifier ()
@property (nonatomic, assign) CGFloat anglePerSecond;
@end

@implementation IDRotationModifier

+ (instancetype)modifierWithRandomAnglePerSecondInRange:(NSRange)range{
    return [self modifierWithAnglePerSecond:( ((arc4random() % 100)/100.0) * range.length + range.location )];
}
+ (instancetype)modifierWithAnglePerSecond:(CGFloat)anglePerSecond{
    IDRotationModifier *modifier = [IDRotationModifier new];
    modifier.anglePerSecond = anglePerSecond;
    return modifier;
}

- (void)modifyLayer:(CALayer *)layer atRelativeTime:(double)relativeTime{
    layer.transform = CATransform3DConcat(layer.transform, CATransform3DMakeRotation(relativeTime * self.anglePerSecond, 0, 0, 1));
}

@end
