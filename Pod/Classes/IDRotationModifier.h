//
//  RotationModifier.h
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "IDConfettoModifier.h"

@interface IDRotationModifier : IDConfettoModifier

+ (instancetype)modifierWithRandomAnglePerSecondInRange:(NSRange)range;
+ (instancetype)modifierWithAnglePerSecond:(CGFloat)anglePerSecond;

@end
