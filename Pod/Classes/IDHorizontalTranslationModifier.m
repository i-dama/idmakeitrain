//
//  HorizontalTranslationModifier.m
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "IDHorizontalTranslationModifier.h"

@interface IDHorizontalTranslationModifier ()
@property (nonatomic, assign) CGFloat stepXTranslation;
@end

@implementation IDHorizontalTranslationModifier

+ (instancetype)modifierWithRandomTranslationFrom:(CGFloat)from to:(CGFloat)to{
    NSInteger range = to - from;
    return [self modifierWithTranslation:(from + (arc4random() % range))];
}

+ (instancetype)modifierWithTranslation:(CGFloat)translation{
    IDHorizontalTranslationModifier *modifier = [IDHorizontalTranslationModifier new];
    modifier.stepXTranslation = translation;
    return modifier;
}

- (void)modifyLayer:(CALayer *)layer atRelativeTime:(double)relativeTime{
    layer.transform = CATransform3DConcat(layer.transform, CATransform3DMakeTranslation(relativeTime * self.stepXTranslation, 0, 0));
}

@end
