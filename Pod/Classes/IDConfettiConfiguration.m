//
//  ConfettiConfiguration.m
//  VriendenLoterij
//
//  Created by Dama on 04/08/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "IDConfettiConfiguration.h"

@implementation IDConfettiConfiguration

+ (instancetype)sharedConfiguration
{
    static IDConfettiConfiguration *configuration;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        configuration = [[IDConfettiConfiguration alloc] init];
    });
    return configuration;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _numberOfConfettos = 60;
        _confettiWidth = 40.0;
        _confettiHeight = 40.0;
    }
    return self;
}

@end
