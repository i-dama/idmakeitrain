//
//  UIView+Confetti.m
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import "UIView+Confetti.h"
#import <objc/runtime.h>
#import "IDConfettiCoordinator.h"
#import "IDConfettoLayer.h"
#import "IDConfettiConfiguration.h"

static char confettiCoordinatorKey;

@implementation UIView (Confetti)

- (void)makeItRainWithCompletion:(void (^)())completion{
    //make some confetti
    NSMutableArray *confetti = [NSMutableArray array];
    NSInteger numberOfConfettos = [IDConfettiConfiguration sharedConfiguration].numberOfConfettos;
    for(int i = 0; i < numberOfConfettos; i++){
        CALayer *confetto = [IDConfettoLayer createConfettoWithRandomizedModifiersAndPositionForView:self];
        [self.layer addSublayer:confetto];
        [confetti addObject:confetto];
    }
    IDConfettiCoordinator *coordinator = [IDConfettiCoordinator confettiCoordinatorWithConfetti:confetti completion:completion];
    objc_setAssociatedObject(self, &confettiCoordinatorKey, coordinator, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [coordinator startInView:self];
}

- (void)cleanUpTheMess{
    IDConfettiCoordinator *coordinator = objc_getAssociatedObject(self, &confettiCoordinatorKey);
    if(coordinator){
        objc_setAssociatedObject(self, &confettiCoordinatorKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [coordinator endInView:self];
    }
    for(int i = self.layer.sublayers.count - 1; i >= 0; i --){
        CALayer *sublayer = self.layer.sublayers[i];
        if([sublayer isKindOfClass:[IDConfettoLayer class]]){
            [sublayer removeFromSuperlayer];
        }
    }
}

+ (IDConfettiCoordinator *)coordinatorForView:(UIView *)view{
    return objc_getAssociatedObject(view, &confettiCoordinatorKey);
}

@end
