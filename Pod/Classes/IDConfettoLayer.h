//
//  ConfettiLayer.h
//  EnergieMonitor
//
//  Created by Dama on 26/11/14.
//  Copyright (c) 2014 Oxxio. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "IDConfettoModifier.h"
#import <UIKit/UIKit.h>

@interface IDConfettoLayer : CALayer

+ (instancetype)createConfettoWithRandomizedModifiersAndPositionForView:(UIView *)view;
+ (instancetype)createConfettoWithModifiers:(NSArray *)modifiers atStartPosition:(CGPoint)startPosition;

@property (nonatomic, strong, readonly) NSArray *confettiModifiers;

@end
