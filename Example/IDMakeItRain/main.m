//
//  main.m
//  IDMakeItRain
//
//  Created by Ivan Damjanović on 08/04/2015.
//  Copyright (c) 2015 Ivan Damjanović. All rights reserved.
//

@import UIKit;
#import "IDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IDAppDelegate class]));
    }
}
