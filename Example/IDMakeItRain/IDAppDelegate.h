//
//  IDAppDelegate.h
//  IDMakeItRain
//
//  Created by Ivan Damjanović on 08/04/2015.
//  Copyright (c) 2015 Ivan Damjanović. All rights reserved.
//

@import UIKit;

@interface IDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
