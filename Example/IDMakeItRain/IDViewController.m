//
//  IDViewController.m
//  IDMakeItRain
//
//  Created by Ivan Damjanović on 08/04/2015.
//  Copyright (c) 2015 Ivan Damjanović. All rights reserved.
//

#import "IDViewController.h"
#import <IDMakeItRain/UIView+Confetti.h>

@interface IDViewController ()

@end

@implementation IDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view makeItRainWithCompletion:nil];
}

@end
