Pod::Spec.new do |s|
  s.name             = "IDMakeItRain"
  s.version          = "0.1.0"
  s.summary          = "Make your views rain confetti"
  s.description      = <<-DESC
                       Allows any UIView to rain some confetti from the top.
                       DESC
  s.homepage         = "https://bitbucket.org/i-dama/idmakeitrain"
  s.license          = 'MIT'
  s.author           = { "Ivan Damjanović" => "ivan.damjanovic@infinum.hr" }
  s.source           = { :git => "git@bitbucket.org:i-dama/idmakeitrain.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/damaofficial'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'

  s.frameworks = 'UIKit'
  s.dependency 'pop'
end

