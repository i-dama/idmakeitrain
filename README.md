# IDMakeItRain

[![CI Status](http://img.shields.io/travis/Ivan Damjanović/IDMakeItRain.svg?style=flat)](https://travis-ci.org/Ivan Damjanović/IDMakeItRain)
[![Version](https://img.shields.io/cocoapods/v/IDMakeItRain.svg?style=flat)](http://cocoapods.org/pods/IDMakeItRain)
[![License](https://img.shields.io/cocoapods/l/IDMakeItRain.svg?style=flat)](http://cocoapods.org/pods/IDMakeItRain)
[![Platform](https://img.shields.io/cocoapods/p/IDMakeItRain.svg?style=flat)](http://cocoapods.org/pods/IDMakeItRain)

## Usage

Be sure to set the images for the confetti in the ```[IDConfettiConfiguration sharedConfiguration]``` object.

Make it rain by calling the ```makeItRainWithCompletion:``` method declared in the ```UIView+Confetii.h``` file.

## Requirements

## Installation

IDMakeItRain is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "IDMakeItRain"
```

## Author

Ivan Damjanović, ivan.damjanovic@infinum.hr

## License

IDMakeItRain is available under the MIT license. See the LICENSE file for more info.